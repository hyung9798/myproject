package com.koscom.tradingbook.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.koscom.tradingbook.entity.SubAccounts;

@Repository
public interface SubAccountsRepository extends JpaRepository<SubAccounts, String>{

	Optional<SubAccounts> findByAccountNumber(String subAccountNo);

}

package com.koscom.tradingbook.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.koscom.tradingbook.entity.TradingBook;

import lombok.NonNull;

public interface TradingBookRepository extends JpaRepository<TradingBook, String>{

	List<TradingBook> findByTradeDateAndAccountNo(String tradeDate, String accountNo);
	
	int countByTradeDateAndAccountNo(@NonNull String tradeDate, @NonNull String accountNo);

}
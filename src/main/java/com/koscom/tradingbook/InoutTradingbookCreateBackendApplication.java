package com.koscom.tradingbook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InoutTradingbookCreateBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(InoutTradingbookCreateBackendApplication.class, args);
	}

}

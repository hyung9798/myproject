package com.koscom.tradingbook.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.koscom.tradingbook.entity.TradingBook;
import com.koscom.tradingbook.exception.SubAccountNoException;
import com.koscom.tradingbook.model.RequestInOutCash;
import com.koscom.tradingbook.repository.TradingBookRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TradebookMakingService {

	@Autowired
	private SubAccountNoService subAccountNoService;

	@Autowired
	private  TradingBookRepository tradingBookRepository;

	public void createTradingBook(RequestInOutCash repInOutCash, TradingBook tradingBook) throws SubAccountNoException {
		// TODO Auto-generated method stub

		log.info("Input Data = ", repInOutCash);
		long  countTB = 0;
		try {
			subAccountNoService.check(repInOutCash.getSubAccountNo());
        } catch (SubAccountNoException e) {
        	throw e;
        }

	    tradingBook.setTradeType(repInOutCash.getInOutFlag());
		
	    tradingBook.setTradeDate(repInOutCash.getTradeDate());
	    tradingBook.setAccountNo(repInOutCash.getSubAccountNo());
	    tradingBook.setTradeNo(repInOutCash.getInOutNo());

	    /* 거래번호(관리를 해야하지만 출납번호를 사용한다.) 
         * 거래일자 , 계좌번호 단위로 거래번호 채번
         * */	    

	    tradingBook.setTradeAmt(repInOutCash.getInOutAmt());
	    tradingBook.setBeforeDeposit(repInOutCash.getBeforeDeposit());
	    tradingBook.setAfterDeposit(repInOutCash.getAfterDeposit());
	    
	    tradingBook.setTradeVal(0);
	    tradingBook.setBeforeBalanceVal(0);
	    tradingBook.setAfterBalanceVal(0);
	    tradingBook.setCodeAccount(repInOutCash.getCodeAccount());
	    tradingBook.setNameAccount(repInOutCash.getNameAccount());
	    tradingBook.setCodeBrief(repInOutCash.getCodeBrief());
	    tradingBook.setNameBrief(repInOutCash.getNameBrief());
	    tradingBook.setReceivableAccruedAmt(repInOutCash.getReceivableAccruedAmt());
	    tradingBook.setReceivableRepayAmt(repInOutCash.getReceivableRepayAmt());
	    tradingBook.setOverDuePenaltyRefundAmt(repInOutCash.getOverDuePenaltyRefundAmt());
	    tradingBook.setTradeTex(0);
	    
		log.info("Insert Data = ", tradingBook);
	   
        try {
        	tradingBookRepository.save(tradingBook);
            if (tradingBook != null) {
                log.info(tradingBook.getAccountNo() + ", 고객 거래내역 생성을 완료했다.");
            }
        } catch (RuntimeException e) {
            log.error("TODO: 고객 거래내역 저장에 문제가 발생했다.");
        }		
	}
}

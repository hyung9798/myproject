package com.koscom.tradingbook.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.koscom.tradingbook.entity.SubAccounts;
import com.koscom.tradingbook.exception.NotFoundSubAccountNoException;
import com.koscom.tradingbook.exception.SubAccountNoException;
import com.koscom.tradingbook.repository.SubAccountsRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SubAccountNoService {

    @Autowired
    private  SubAccountsRepository  subAccountsRepository;
    
	public void check(String subAccountNo) throws SubAccountNoException {

        log.info(" This Point is Sub-account check routine");            	
		try {
			Optional<SubAccounts> optional = subAccountsRepository.findByAccountNumber(subAccountNo);
            if (optional.isPresent()) {
                log.info(subAccountNo + ", 개별 계좌 정보 가져온다 ..");            	
            }else {
                log.info(" Select but not optional.isPresent()");            	
            	throw new NotFoundSubAccountNoException();
            }
		} catch (Exception e) {
            log.info(" Select but exception e");            	
			throw new NotFoundSubAccountNoException();
		}
	}	
}

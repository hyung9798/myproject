package com.koscom.tradingbook.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.koscom.tradingbook.entity.TradingBook;
import com.koscom.tradingbook.exception.NotFoundTradingListException;
import com.koscom.tradingbook.exception.SubAccountNoException;
import com.koscom.tradingbook.model.ResponseTradingBookList;
import com.koscom.tradingbook.repository.TradingBookRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TradingInfoListService {

    @Autowired
    private  SubAccountNoService  subAccountNoService;

    @Autowired
    private  TradingBookRepository  tradingBookRepository;
   
	public Object getTradingInfoList(String accountNo, String tradeDate, int tradeNo) throws SubAccountNoException {
		// TODO Auto-generated method stub       

	    ResponseTradingBookList resTradingBookList  =  new ResponseTradingBookList();

		log.info("This Point is getTradeInfoList Routine..");
		
	log.info("test ......................................");
       try {
			subAccountNoService.check(accountNo);
        } catch (SubAccountNoException e) {
            throw e;
        }

        log.info("SubAccountNo=" +  accountNo + "TradeDate=" + tradeDate);
        
        try {
        	List <TradingBook> optional = tradingBookRepository.findByTradeDateAndAccountNo(tradeDate,accountNo);
           	log.info(tradeDate + accountNo + "," + "의 개별 계좌 출납내역 정보 가져온다 ..");
       		if (optional.isEmpty()) {
       			throw new NotFoundTradingListException();
            } else {
            	resTradingBookList.setListTradingBook(optional);  	
            	return  resTradingBookList;
            }
        } catch (Exception e) {
            throw new  NotFoundTradingListException();
        }
	}
}

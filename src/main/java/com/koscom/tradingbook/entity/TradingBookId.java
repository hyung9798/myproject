package com.koscom.tradingbook.entity;

import java.io.Serializable;
import java.util.Objects;

import lombok.Data;

@Data
public class TradingBookId implements Serializable {
	
    private String tradeDate;
    private String accountNo;
    private long tradeNo;
	
	public TradingBookId() {
	}
	
	public TradingBookId(String tradeDate,	String accountNo, long tradeNo) {
		this.tradeDate = tradeDate;
		this.accountNo = accountNo;
		this.tradeNo = tradeNo;
	}
	@Override
	public boolean equals(Object o) {
		if(this == o) return true;
		if(o == null  ||  getClass() !=o.getClass()) return false;
		TradingBookId	tradingBookId	= (TradingBookId) o;
		return tradeDate.equals(tradingBookId.tradeDate)  &&
				accountNo.equals(tradingBookId.accountNo)  &&
				tradeNo == tradingBookId.tradeNo;
	}
	@Override
	public	int	hashCode() {
		return	Objects.hash(tradeDate, accountNo, tradeNo);
	}
		
}


package com.koscom.tradingbook.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@Table(name = "sub_accounts")
public class SubAccounts {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("Id")
    @Column(name = "id", nullable = false, length = 11 )
	private int id						;	//고객ID

	@NonNull
	@JsonProperty("AccountNumber")
	@Column(name = "account_number", nullable = false, length = 11)
	private String accountNumber		;  	//개별계좌ID(개별계좌번호)

	@NonNull
	@JsonProperty("AccountName")
	@Column(name = "account_name", nullable = false, length = 40)
	private String accountName			;  	//계좌명

	@NonNull
	@JsonProperty("type")
	@Column(name = "type", nullable = false, length = 2)
	private String type					;  	//업무구분(01.위탁자 ...)

	@NonNull
	@JsonProperty("TradingTax")
	@Column(name = "trading_tax", nullable = false, length = 1)
	private String tradingTax			;  	//증거금징수여부(0.징수, 1.미징수)

	@NonNull
	@JsonProperty("IsPreMargin")
	@Column(name = "is_pre_margin", nullable = false, length = 1)
	private String isPreMargin			;  	//거래세과세여부(0.과세, 1.비과세)

	@JsonProperty("MasterAccountId")
	@Column(name = "master_account_id", nullable = false, length = 11)
	private int masterAccountId			; 	//종합계좌ID	
}

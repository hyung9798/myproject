package com.koscom.tradingbook.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@Table(name = "trading_book")
@IdClass(TradingBookId.class)

public class TradingBook implements Serializable{

//    @ApiModelProperty("일련번호")
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @JsonProperty("SerialId")
//    @Column(name = "serial_no", updatable = false, nullable = false)
//    private long  serialId;
//
    @ApiModelProperty(value = "거래일자", example = "20200518")
    @NonNull
    @Id
    @JsonProperty("TradeDate")
    @Column(name = "trade_date", nullable = false, length = 8)
    private String tradeDate;

    @ApiModelProperty(value ="계좌번호") /* 종합계좌번호, 개별계좌번호 구분 없음 */
    @NonNull
    @Id
    @JsonProperty("AccountNo")
    @Column(name = "account_no", nullable = false, length = 20)
    private String accountNo;


    @ApiModelProperty(value ="거래번호")
    @JsonProperty("TradeNo")
    @Id
    @Column(name = "trade_no", nullable = false)
    private long tradeNo;

    @ApiModelProperty(value ="거래구분")
    @JsonProperty("TradeType")
    @Column(name = "trade_type", nullable = false, length = 2)
    private String tradeType;

    @ApiModelProperty(value ="거래금액")
    @JsonProperty("TradeAmt")
    @Column(name = "trade_amt", nullable = false)
    private long tradeAmt;

    @ApiModelProperty(value ="거래전예수금")
    @JsonProperty("BeforeDeposit")
    @Column(name = "before_deposit", nullable = false)
    private long beforeDeposit;

    @ApiModelProperty(value ="거래후예수금")
    @JsonProperty("AfterDeposit")
    @Column(name = "after_deposit", nullable = false)
    private long afterDeposit;

    @ApiModelProperty(value ="거래수량")
    @JsonProperty("TradeVal")
    @Column(name = "trade_val", nullable = false)
    private long tradeVal;

    @ApiModelProperty(value ="거래전잔고수량")
    @JsonProperty("BeforeBalanceVal")
    @Column(name = "before_balance_val", nullable = false)
    private long beforeBalanceVal;

    @ApiModelProperty(value ="거래후잔고수량")
    @JsonProperty("AfterBalanceVal")
    @Column(name = "after_balance_val", nullable = false)
    private long afterBalanceVal;

    @ApiModelProperty(value ="계정코드")
    @NonNull
    @JsonProperty("CodeAccount")
    @Column(name = "code_account", nullable = false, length =4)
    private String codeAccount;

    @ApiModelProperty(value ="계정명")
    @NonNull
    @JsonProperty("NameAccount")
    @Column(name = "name_account", nullable = false, length =40)
    private String nameAccount;

    @ApiModelProperty(value ="적요코드")
    @NonNull
    @JsonProperty("CodeBrief")
    @Column(name = "code_brief", nullable = false, length = 4)
    private String codeBrief;

    @ApiModelProperty(value ="적요명")
    @NonNull
    @JsonProperty("NameBrief")
    @Column(name = "name_brief", nullable = false, length =40)
    private String nameBrief;

    @ApiModelProperty(value ="미수발생금액")
    @JsonProperty("ReceivableAccruedAmt")
    @Column(name = "receivable_accrued_amt", nullable = false)
    private long receivableAccruedAmt;
    
    @ApiModelProperty(value ="미수상환금액")
    @JsonProperty("ReceivableRepayAmt")
    
    @Column(name = "receivable_repay_amt", nullable = false)
    private long receivableRepayAmt;
    
    @ApiModelProperty("미수연체료상환금액")
    @JsonProperty("OverDuePenaltyRefundAmt")
    @Column(name = "over_due_penalty_refund_amt", nullable = false)
    private long overDuePenaltyRefundAmt;
    
    @ApiModelProperty(value = "거래세")
    @JsonProperty("TradeTex")
    @Column(name = "trade_tex", nullable = false)
    private long  tradeTex;
}

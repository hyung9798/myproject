package com.koscom.tradingbook.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.koscom.tradingbook.service.TradebookMakingService;


@Configuration
public class TradingBookMakingServiceConfig {

    @Bean
    @Profile("default | koscom")
    public TradebookMakingService  tradebookMakingService() {
        return new  TradebookMakingService();
    }
}
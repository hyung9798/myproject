package com.koscom.tradingbook.exception;

import org.springframework.http.HttpStatus;

public class NotFoundTradingListException extends SubAccountNoException {

    public NotFoundTradingListException() {
        super("개별계좌번호를 찾을 수가 없습니다.");
        errorCode = HttpStatus.NOT_FOUND;
    }

}

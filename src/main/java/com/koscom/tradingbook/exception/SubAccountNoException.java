package com.koscom.tradingbook.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@NoArgsConstructor
public class SubAccountNoException extends Exception {

    HttpStatus errorCode;

    public SubAccountNoException(String name) {
        super(name);
    }
}

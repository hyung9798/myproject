package com.koscom.tradingbook.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class SubAccountNoValidationException extends Exception {

    HttpStatus errorCode = HttpStatus.BAD_REQUEST;

    public SubAccountNoValidationException(String name) {
        super(name);
    }
}

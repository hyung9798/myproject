package com.koscom.tradingbook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.koscom.tradingbook.entity.TradingBook;
import com.koscom.tradingbook.exception.SubAccountNoException;
import com.koscom.tradingbook.model.ErrorMessage;
import com.koscom.tradingbook.model.RequestInOutCash;
import com.koscom.tradingbook.model.ResponseTradingBookList;
import com.koscom.tradingbook.service.TradebookMakingService;
import com.koscom.tradingbook.service.TradingInfoListService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api")
@Api(value = "cashdeposit", description = "거래내역 생성 처리 API")
public class TradingBookController {


    @Autowired
    private  TradebookMakingService  tradebookMakingService;

    @Autowired
    private  TradingInfoListService  tradingInfoListService;

    TradingBook  tradingBook  =  new  TradingBook();

/* 입금 처리 */  
    @PostMapping(value = "/trade")
    @ApiOperation("거래내역 생성 처리")
    @ApiResponses({
        @ApiResponse(code = 200, message = "거래내역 생성 처리 성공", response = TradingBook.class),
        @ApiResponse(code = 400, message = "거래내역 생성 처리 실패", response = ErrorMessage.class),
        @ApiResponse(code = 401, message = "계좌번호 인증 실패", response = ErrorMessage.class),
        @ApiResponse(code = 404, message = "계좌번호 확인 불가", response = ErrorMessage.class)
    })
    public ResponseEntity<?> MakeTradingAction(@RequestBody RequestInOutCash repInOutCash) throws Exception {
    	log.info("This point is CashInAction(Controller) param RequestInCash={" + repInOutCash + "}");
        try {
        	tradebookMakingService.createTradingBook(repInOutCash, tradingBook);
            return new ResponseEntity<>(tradingBook, HttpStatus.OK);
        } catch (SubAccountNoException e) {
            log.warn(e.getMessage());
            return new ResponseEntity<>(new ErrorMessage(e.getMessage()), e.getErrorCode());
        }
    }

    /* 거래내역 가져오기 */
    @GetMapping(value = "/trade")
    @ApiOperation("거래내역 정보 가져오기")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "SubAccountNo", value = "개별계좌번호", required = false, dataType = "String", paramType = "query", defaultValue = "00101000001"),
        @ApiImplicitParam(name = "TradeDate", value = "거래일자", required = false, dataType = "String", paramType = "query", defaultValue = "20200518"),
        @ApiImplicitParam(name = "TradeNo", value = "거래번호", required = false, dataType = "String", paramType = "query", defaultValue = "1")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "거래내역 정보 가져오기 성공", response = ResponseTradingBookList.class),
        @ApiResponse(code = 400, message = "거래내역 정보 없음", response = ErrorMessage.class),
        @ApiResponse(code = 401, message = "계좌번호 인증 실패", response = ErrorMessage.class),
        @ApiResponse(code = 404, message = "계좌번호 확인 불가", response = ErrorMessage.class)
    })
    public ResponseEntity<?> getTradeInfoList(@RequestParam("SubAccountNo") String  subAccountNo,
    		@RequestParam("TradeDate") String  tradeDate, @RequestParam("TradeNo") int  tradeNo)   throws Exception  {
        try {
            return new ResponseEntity<>(tradingInfoListService.getTradingInfoList(subAccountNo,tradeDate,tradeNo), HttpStatus.OK);
        } catch (SubAccountNoException e) {
            log.warn(e.getMessage());
            return new ResponseEntity<>(new ErrorMessage(e.getMessage()), e.getErrorCode());
        }
    }

}


package com.koscom.tradingbook.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@ApiModel("에러메시지 정의")
@NoArgsConstructor
@RequiredArgsConstructor
public class ErrorMessage {

    @ApiModelProperty("에러메시지")
    @NonNull
    @JsonProperty("ErrMsg")
    private String message;
}

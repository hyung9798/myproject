package com.koscom.tradingbook.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@ApiModel("현금입출금처리")
@NoArgsConstructor
@RequiredArgsConstructor
public class RequestInOutCash {

	@ApiModelProperty(value ="개별계좌번호",example="00101000001")
	@NonNull
	@JsonProperty("SubAccountNo")	
	private String subAccountNo;

    @ApiModelProperty(value ="거래일자",example="20200518")
    @NonNull
    @JsonProperty("TradeDate")
    private String  tradeDate;
	
	@ApiModelProperty(value ="출납번호")
	@JsonProperty("InOutNo") 
	private long inOutNo;

    @ApiModelProperty(value ="출납구분",example="01")
    @NonNull
    @JsonProperty("InOutFlag")
    private String  inOutFlag;

    @ApiModelProperty(value ="출납금액")
	@JsonProperty("InOutAmt")
	private long inOutAmt;

    @ApiModelProperty(value ="변경전잔고금액")
	@JsonProperty("BeforeDeposit")
	private long beforeDeposit;

    @ApiModelProperty(value ="변경후잔고금액")
	@JsonProperty("AfterDeposit")
	private long afterDeposit;

	@ApiModelProperty(value ="계정코드",example="01")
	@NonNull
	@JsonProperty("CodeAccount")
	private String codeAccount;

	@ApiModelProperty(value ="계정명",example="출금")
	@NonNull
	@JsonProperty("NameAccount")
	private String nameAccount;

	@ApiModelProperty(value ="적요코드",example="01")
	@NonNull
	@JsonProperty("CodeBrief")
	private String codeBrief;

	@ApiModelProperty(value ="적요명",example="창구출금")
	@NonNull
	@JsonProperty("NameBrief")
	private String nameBrief;

	@ApiModelProperty(value ="미수발생금액")
	@JsonProperty("ReceivableAccruedAmt")
	private long receivableAccruedAmt;

	@ApiModelProperty(value ="미수상환금액")
	@JsonProperty("ReceivableRepayAmt")
	private long receivableRepayAmt;

	@ApiModelProperty(value ="연체료상환금액")
	@JsonProperty("OverDuePenaltyRefundAmt")
	private long overDuePenaltyRefundAmt;

}

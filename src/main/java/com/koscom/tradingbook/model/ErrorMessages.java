package com.koscom.tradingbook.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@ApiModel("복수개의 에러메시지 정의")
@NoArgsConstructor
@RequiredArgsConstructor
public class ErrorMessages {

    @ApiModelProperty("복수개의 에러메시지")
    @NonNull
    @JsonProperty("ErrMsgs")
    private List<ErrorMessage> messages;
}

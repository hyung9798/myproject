package com.koscom.tradingbook.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.koscom.tradingbook.entity.TradingBook;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("거래정보 내역")
public class ResponseTradingBookList {

	@ApiModelProperty("거래내역List")
    @JsonProperty("ListTradingBook")
	List<TradingBook> listTradingBook = new  ArrayList<TradingBook>();
}
